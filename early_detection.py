import pandas as pd
import numpy as np
import statsmodels as sm
from common_functions import *
from pandas.tools.plotting import autocorrelation_plot
from pylab import *
import statsmodels.tsa.api as tsa


def forecast(data):

    model = tsa.VAR(data)
    lag_order = model.select_order(verbose=False)
    results = model.fit(maxlags=lag_order['aic'], ic='aic')
    pred = results.forecast(data.values[-results.k_ar:], 6)
    
    return pred

#------------------------------------------------------------------------------------------------

# check bs
bs_alert_trckr = []

def check_bs(obs, pred):

    bs_ul = np.mean(pred[:, 0]) + (1.96 * np.std(pred[:, 0])) #bs threshold
#     print(bs_ul)
    
    if obs > bs_ul:
        if len(bs_alert_trckr) == 0:
            bs_alert_trckr.append(obs)
        elif len(bs_alert_trckr) > 0 and len(bs_alert_trckr) < 6:
            if True in any(i[0]=='T' for i in bs_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if bs_alert_trckr[j][0] == 'T' and obs >= bs_alert_trckr[j][1]: 
                        print('increasing bs score')
                        break
                    elif bs_alert_trckr[j][0] == 'T' and obs < bs_alert_trckr[j][1]:
                        print('recovering bs')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes for bs')
            bs_alert_trckr.append(('T', obs))
        elif len(bs_alert_trckr) >= 6:
            if True in any(i[0]=='T' for i in bs_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if bs_alert_trckr[j][0] == 'T' and obs >= bs_alert_trckr[j][1]: 
                        print('increasing bs score')
                        break
                    elif bs_alert_trckr[j][0] == 'T' and obs < bs_alert_trckr[j][1]:
                        print('recovering bs')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes for bs')
            bs_alert_trckr.pop(0) 
            bs_alert_trckr.append(('T', obs))
    else:
        if len(bs_alert_trckr) < 6:
            bs_alert_trckr.append(('F', obs))
            print('bs is normal')
        else:
            bs_alert_trckr.pop(0)
            bs_alert_trckr.append(('F', obs))
            print('bs is normal')
            
#     print(bs_alert_trckr)

#------------------------------------------------------------------------------------------------

# check fe
fe_alert_trckr = []

def check_fe(obs, pred):

    fe_ul = np.mean(pred[:, 0]) + (1.96 * np.std(pred[:, 0])) #bs threshold
#     print(fe_ul)
    
    if obs > fe_ul:
        if len(fe_alert_trckr) == 0:
            fe_alert_trckr.append(obs)
        elif len(fe_alert_trckr) > 0 and len(fe_alert_trckr) < 6:
            if True in any(i[0]=='T' for i in fe_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if fe_alert_trckr[j][0] == 'T' and obs >= fe_alert_trckr[j][1]: 
                        print('increasing fe score')
                        break
                    elif fe_alert_trckr[j][0] == 'T' and obs < fe_alert_trckr[j][1]:
                        print('recovering fe')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes from fe')
            fe_alert_trckr.append(('T', obs))
        elif len(fe_alert_trckr) >= 6:
            if True in any(i[0]=='T' for i in fe_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if fe_alert_trckr[j][0] == 'T' and obs >= fe_alert_trckr[j][1]: 
                        print('increasing fe score')
                        break
                    elif fe_alert_trckr[j][0] == 'T' and obs < fe_alert_trckr[j][1]:
                        print('recovering fe')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes from fe')
            fe_alert_trckr.pop(0) 
            fe_alert_trckr.append(('T', obs))
    else:
        if len(fe_alert_trckr) < 6:
            fe_alert_trckr.append(('F', obs))
            print('fe is normal')
        else:
            fe_alert_trckr.pop(0)
            fe_alert_trckr.append(('F', obs))
            print('fe is normal')
            
#     print(fe_alert_trckr)

#------------------------------------------------------------------------------------------------

# check ds
ds_alert_trckr = []

def check_ds(obs, pred):

    ds_ul = np.mean(pred[:, 2]) + (1.96 * np.std(pred[:, 2])) #bs threshold
#     print(bs_ul)
    
    if obs > ds_ul:
        if len(ds_alert_trckr) == 0:
            ds_alert_trckr.append(obs)
        elif len(ds_alert_trckr) > 0 and len(ds_alert_trckr) < 6:
            if True in any(i[0]=='T' for i in ds_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if ds_alert_trckr[j][0] == 'T' and obs >= ds_alert_trckr[j][1]: 
                        print('increasing ds score')
                        break
                    elif ds_alert_trckr[j][0] == 'T' and obs < ds_alert_trckr[j][1]:
                        print('recovering')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes for ds')
            ds_alert_trckr.append(('T', obs))
        elif len(ds_alert_trckr) >= 6:
            if True in any(i[0]=='T' for i in ds_alert_trckr):
                for j in range(-1, -(len(ds_alert_trckr)+1), -1):
                    if ds_alert_trckr[j][0] == 'T' and obs >= ds_alert_trckr[j][1]: 
                        print('increasing ds score')
                        break
                    elif ds_alert_trckr[j][0] == 'T' and obs < ds_alert_trckr[j][1]:
                        print('recovering ds')
                        break
            else:
                print('alert, possible DRM outage the next 30 minutes for ds')
            ds_alert_trckr.pop(0) 
            ds_alert_trckr.append(('T', obs))
    else:
        if len(ds_alert_trckr) < 6:
            ds_alert_trckr.append(('F', obs))
            print('ds is normal')
        else:
            ds_alert_trckr.pop(0)
            ds_alert_trckr.append(('F', obs))
            print('ds is normal')
            
#     print(ds_alert_trckr)

#------------------------------------------------------------------------------------------------

def detect(data, start):

    from time import sleep

    start = start
    end = start + 20
    pred = forecast(data[:3401])
    count = 1

    while start < end:
        while count <= 6:
            check_bs(data.iloc[start: start + 1, 0][0], pred)
            check_fe(data.iloc[start: start + 1, 1][0], pred)
            check_ds(data.iloc[start: start + 1, 2][0], pred)
            start += 1
            count += 1
            sleep(2)
            print('-------------------------------')
        pred = forecast(data[:start + 6])
        count = 1


