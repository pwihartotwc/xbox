import pandas as pd
import numpy as np
import itertools
from scipy.stats.stats import pearsonr
from sklearn_pandas import DataFrameMapper
from sklearn.preprocessing import MinMaxScaler, StandardScaler, LabelEncoder


def read_data(filename, sheetname, parse_cols, float_start, float_end):

    df = pd.read_excel(filename, sheetname=sheetname, parse_cols=parse_cols)
    df = df.round(3)
    df.iloc[:, float_start:float_end].astype(float, copy=False)
    df.dropna(inplace=True)
    
    return df

#-----------------------------------------------------------------------------------------

def outliers_gone(df, col_start, col_end):

    cols = df.iloc[:, col_start:col_end].columns.tolist()

    for i in cols:
        q1 = df.loc[:, i].quantile(0.25)
        q3 = df.loc[:, i].quantile(0.75)
        if q1 == 0 and q3 == 0:
            df.loc[:, i] = df.loc[:, i]
        else:
            iqr = q3 - q1
            df = df[df[i] < (q3 + (1.5 * iqr))]
        
    return df

#-----------------------------------------------------------------------------------------

def find_cc(df, col_start, col_end):

    cols = list(df.iloc[:, col_start:col_end+1].columns.values)

    cc_dict = {}

    for i, j in itertools.combinations(cols, 2):
        cc_dict[i+ '__' +j] = np.round(pearsonr(df.loc[:, i], df.loc[:, j]), 3)

    rho = pd.DataFrame.from_dict(cc_dict, orient='index')
    rho.columns = ['CC', 'p-value']
    
    return rho

#-----------------------------------------------------------------------------------------

def norm_std(good, bad):

    frames = [good, bad]
    combo = pd.concat(frames)
    #combo = combo.iloc[np.random.permutation(len(combo))].reset_index(drop=True)      # shuffle rows
    cols = ['bs_score', 'fe_score', 'ds_score_live', 'ds_score_vod']
    combo[cols] = np.round(MinMaxScaler().fit_transform(combo[cols]),3)
    combo[cols] = np.round(StandardScaler().fit_transform(combo[cols]),3)

    return combo

#-----------------------------------------------------------------------------------------

def py_datetime(df):

    df['minute'] = (df['minute']/1000).astype('int').astype('datetime64[s]')

    return df
    
